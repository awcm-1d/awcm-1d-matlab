function [F_out, X, L_out]  = adapt_grid(Fn, L)
%ADAPT_GRID using global definitions
%
% Fn         is the function 
%
% Written: Oleg V. Vasilyev, 14 October, 2018

global ende G j_df Type jlev Jmx scl acc order h hh XX x

Fn   = wtf(Type, Fn, jlev, order, 'forward', L);
Fn(~L)=0.0;

L_sig = significant(Fn, L, scl, acc, jlev);

[Fn, L_sig, jlev, ende] = adjust_lev(Fn, L_sig, jlev, Jmx);

L_sig = add_adjacent(Type, L_sig, jlev);

L = reconstruction_check(Type, jlev, order, L_sig);

[G, j_df] = add_ghost(L, Type, jlev, order);

s      = 2^(Jmx-jlev);
h      = hh*s;                           %h - mesh spacing on the finest level
X      = XX(1:s:end);                    %X - grid on finest level

L_out = L;
F_out = wtf(Type, Fn, jlev, order, 'inverse', L);
F_out(~L)=NaN;