function dwgh = dwgh(k, n_l, n_h,h)
%DWGH calculates weights for the fisrt derivative calculation for 
%uniformly spaced Lagrange interpolation at X_s point using X_l= l*Delta points (l=(s+n_l)...(s+n_h))
%
% h  - mesh spacing
% k  - Index of the point
% nl - How many point do I want to use to the left  (could be to the right)
% nh - How many point do I want to use to the right (could be to the right)
%
% EXAMPLE
% dwgh( 0,  0, 1, 0.1) - Calculates the weight in point  0 using assymetric
% stencil with 0 points on the left and 1 point on the right with 0.1 spacing
% dwgh(-1, -2, 1, 0.1) - Calculates the weight in point  0 using assymetric
% stencil with 2 points on the left and 1 point on the right with 0.1 spacing
%
% Written: Oleg V. Vasilyev, 4 October, 2018
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
dwgh=0;
range=[n_l:n_h];
i=range(find(range ~= k));
for m=i
    dwgh=dwgh-prod(i(find(i ~= m)));
end
dwgh=dwgh/prod(i-k)/h;


