function N_range_corr = C_range_corr(Ind, n, s, N_range)
%C_RANGE_CORR the bound to ensure the points do not go outside the interval
% Written: Oleg V. Vasilyev, 11 October, 2018

N_range_corr = Ind+s*(2*N_range+1);
N_range_corr = N_range_corr + max(1+s,N_range_corr(1)) - N_range_corr(1);
N_range_corr = N_range_corr + min(n-s,N_range_corr(end))- N_range_corr(end);  
N_range_corr = ((N_range_corr(find(N_range_corr > 0)) - Ind)/s -1)/2;
